#!/usr/bin/env python

# Neo-Retro Group

from Crypto.Cipher import AES
import os
import sys

# the block size for the cipher object; must be 16, 24, or 32 for AES
BLOCK_SIZE = 32

secret = os.urandom(BLOCK_SIZE)
data = os.urandom(BLOCK_SIZE*1024*1024)

cipher = AES.new(secret);

while True:
	sys.stdout.write(data)
	data = cipher.encrypt(data)
